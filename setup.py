import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="digi_oai_eportal_ubit", # Replace with your own username
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="get missing alma dois from e-portal oai-pmh",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=['rdv_marc_ubit', 'cache_decorator_redis_ubit', 'sickle', 'mongo_decorator_ubit', 'rdv_data_helpers_ubit', 'es_ingester_ubit'],
    include_package_data=True,
    zip_safe=False
)