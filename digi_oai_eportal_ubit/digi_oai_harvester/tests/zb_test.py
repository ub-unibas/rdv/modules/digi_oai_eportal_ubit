
from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator
from mongo_decorator_ubit import create_mongo_decorator
from pkg_resources import Requirement, resource_filename
from digi_oai_eportal_ubit.digi_oai_harvester import ecodicesOAI, eraraOAI, emanuscriptaOAI, UBS_SLSP_OAI

MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("mongo_decorator_ubit"),
                                                                      "mongo_decorator_ubit/cfg/mongo_emptyvalues.yaml"))
alma_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator,
                        oai_store=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="no_zb_oai"),
                        oai_set="no_sb_set", oai_metadata="marc21")

# TODO:  Problem dass SRU Records bzw. get Recordsin den OAI-Cache geschrieben werden
#https://www.e-rara.ch/oai?verb=GetRecord&identifier=oai:www.e-rara.ch:9187056&metadataPrefix=mets
#www.e-rara.ch:9187258

erara_oai = "https://www.e-rara.ch/oai"
erara_sets = {"zuz": {}}
erara_oai = eraraOAI(oai_sets=erara_sets,
                     cachedecorator=NoCacheDecorator,
                     oai_store=MongoDecorator(db_name="oai_harvesting", collection_name="erara_zb"),
                     slsp_oai=alma_oai)
if 0:
    # reloads whole dataset, does not use mongo db
    erara_data = erara_oai.full_harvest(oai_set="zuz",
                                        oai_metadata="mets",
                                        xml_process_func=erara_oai.process_eportal_mets)
# loads only new records, last harvest time stamp is always stored in database
erara_data = erara_oai.full_cached_harvest(oai_set="zuz",
                                    oai_metadata="mets",
                                    xml_process_func=erara_oai.process_eportal_mets)
