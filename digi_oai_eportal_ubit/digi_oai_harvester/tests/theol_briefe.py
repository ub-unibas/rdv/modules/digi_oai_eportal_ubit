prefix = ["G I 33",
          "G I 59",
          "G I 60",
          "G I 66",
          "G I 68",
          "G I 69",
          "G II 1",
          "G II 2",
          "G II 3",
          "G II 4",
          "G II 5",
          "G II 6",
          "G II 7",
          "G II 8",
          "G II 9",
          "G II 10",
          "G II 11",
          "G II 12",
          "G II 43",
          "G II 44",
          "G II 45",
          "G II 46",
          "G II 47",
          "G II 48",
          "G II 49",
          "G2 II 1",
          "G2 II 2",
          "G2 II 3",
          "G2 II 4",
          "G2 II 5",
          "Frey-Gryn Mscr I 19",
          "G II 30",
          "G II 31",
          "G2 I 16",
          "G2 I 17",
          "G2 I 20a",
          "G2 I 20b",
          "G2 I 22",
          "G2 I 24",
          "G2 I 28b",
          "G2 I 37",
          "G2 II 20",
          "G2 II 21",
          "G2 II 80",
          "KiAr Mscr 18a",
          "KiAr Mscr 18b",
          "KiAr Mscr 22a",
          "KiAr Mscr 22b",
          "KiAr Mscr 22c",
          "KiAr Mscr 22d",
          "KiAr Mscr 22e",
          "KiAr Mscr 23a",
          "KiAr Mscr 23b",
          "KiAr Mscr 25b",
          "KiAr Mscr 25c",
          "Frey-Gryn Mscr II 9",
          "Frey-Gryn Mscr II 14",
          "Frey-Gryn Mscr II 19"]
from collections import Counter
import csv
c = Counter()
digitized = []
for k, entry in oai_set.items():
    callnums = entry.get("callnumbers", [])
    for callnum in callnums:
        for csplit in callnum.split(":"):
            for p in prefix:
                if csplit.endswith(p):
                    d = {"Signatur beinhaltet": p, "Signatur": callnum,
                         "izid": "https://swisscollections.ch/Record/{}".format([e for e in entry.get("sys_ids") if e.endswith("5504")][0]),
                         "doi": "http://dx.doi.org/{}".format([e for e in entry.get("ids")][0])}
                    c[p] += 1
                    digitized.append(d)
digitized.sort(key= lambda x: (x.get("Signatur beinhaltet"), x.get("SSignatur")))
with open("../../../../digi_eportal_comparision/digi_eportal_comparision/doi_enricher_alma/data/theo_briefe.csv", "w") as theo_fh:
    cswriter = csv.DictWriter(theo_fh, fieldnames=["Signatur beinhaltet", "Signatur", "izid", "doi"])
    cswriter.writeheader()
    for line in digitized:
        cswriter.writerow(line)
print(c.most_common(50))