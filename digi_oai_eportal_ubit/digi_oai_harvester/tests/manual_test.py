from digi_oai_eportal_ubit.digi_oai_harvester import ecodicesOAI, eraraOAI, emanuscriptaOAI, UBS_SLSP_OAI
from cache_decorator_redis_ubit import CacheDecorator
from rdv_marc_ubit import MarcTransformRule, AlmaIZMarcJSONRecord, SWAJSONRecord
from cache_decorator_redis_ubit import NoCacheDecorator
from mongo_decorator_ubit.mongo_decorator import create_mongo_decorator
from pkg_resources import Requirement, resource_filename
import time


#oai:www.e-rara.ch:11295649 nachschauen, ob alles passt
if 1:
    MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"))

    alma_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="swa_sachdok_Test"))
    alma_data = alma_oai.full_cached_harvest(oai_set="swa_sachdok", oai_metadata="marc21", xml_process_func=alma_oai.process_marc, last_harvest_date="2022-03-05")
    print(alma_oai.load_oai_recordcache(oai_id="oai:alma.41SLSP_UBS:9966658440105504", oai_metadata="marc21"))

if 0:
    #9972426390405504, anschauen wegen Manifest: oai:www.e-manuscripta.ch:3282330
    MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"))

    slsp_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="swa_sachdok"))

    slsp_data = slsp_oai.harvest_ids(oai_ids=["oai:alma.41SLSP_UBS:9962816470105504"], oai_set="swa_sachdok",
                                         oai_metadata="marc21", xml_process_func=slsp_oai.process_marc)

    print(slsp_data)

if 0:
    ecodices_sets = {"ubb": {"platform": ["ecodices"]}}
    marc_rule = MarcTransformRule(gd_service_file = "/home/martin/PycharmProjects/rdv_middlelayer/rdv_cfg/rdviewer-1d4d278e4cac.json",
                                  marc_spreadsheet = "1SRwSdueGpxItne4KKLhM6uvsXJVByP879Vxpx_VFPWg", marc_record_class=AlmaIZMarcJSONRecord,
                                  gd_cache_decorator=NoCacheDecorator)
    MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"))
    alma_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="retrodigi"),
                            oai_set="retrodigi", oai_metadata="marc21")

    ecodices = ecodicesOAI(oai_sets=ecodices_sets, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="oai_harvesting", collection_name="ecodices"),
                           alma_oai=alma_oai, marc_rule=marc_rule)
    ecod_data = ecodices.full_cached_harvest(oai_set="ubb", oai_metadata="oai_dc", xml_process_func=ecodices.prepare_digibas_data)
    print(ecod_data)

if 0:
    MongoDecorator = create_mongo_decorator(
        config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"),
        db_name="oai_harvesting", collection_name="erara")
    erara_sets = {"bau_1": {"990_f": "erarabs", "990_9": "LOCAL"}}
    erara_oai = eraraOAI(oai_sets=erara_sets, cachedecorator=CacheDecorator, oai_store=MongoDecorator)
    erara_data = erara_oai.full_cached_harvest(oai_set="bau_1", oai_metadata="mets", xml_process_func=erara_oai.process_eportal_mets)
    print(erara_data)

if 1:
    MongoDecorator = create_mongo_decorator(
        config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"),
        db_name="oai_harvesting", collection_name="emanuscripta")
    emanuscripta_sets = {"emanusswa": {"900_f": "HANemanuscriptabsswa"},
                         "emanusbau": {"990_f": "emanuscriptabs", "990_9": "LOCAL", "900_f": "HANemanuscriptabsub"},
                         "emanusbau1": {"990_f": "emanuscriptabs", "900_f": "HANemanuscriptabsub"},
                         "sbz": {"990_f": "tbd"}}
    MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"))
    alma_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator,
                            oai_db=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="retrodigi"), oai_set="retrodigi", oai_metadata="marc21")

    emanus_oai = emanuscriptaOAI(oai_sets=emanuscripta_sets, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="oai_harvesting", collection_name="emanuscripta"),
                                 alma_oai=alma_oai)
    emanus_data = emanus_oai.full_cached_harvest(oai_set="emanusswa", oai_metadata="mets", xml_process_func=emanus_oai.process_eportal_mets)
    print(emanus_data)
