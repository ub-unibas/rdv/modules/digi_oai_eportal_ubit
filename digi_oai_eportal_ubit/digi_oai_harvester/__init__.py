from digi_oai_eportal_ubit.digi_oai_harvester.oai_harvester import ePortalOAI
from digi_oai_eportal_ubit.digi_oai_harvester.erara_harvester import eraraOAI, emanuscriptaOAI
from digi_oai_eportal_ubit.digi_oai_harvester.ecodices_harvester import ecodicesOAI
from digi_oai_eportal_ubit.digi_oai_harvester.alma_harvester import UBS_SLSP_OAI