import re
import requests
from xml.etree import ElementTree

from rdv_marc_ubit import AlmaIZMarcJSONRecord
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD, IIIF_IMGS_ESFIELD, OLDSYS_ESFIELD, IZID_ESFIELD, \
    DOI_ESFIELD

from digi_oai_eportal_ubit.digi_oai_harvester.oai_harvester import ePortalOAI
from digi_oai_eportal_ubit.digi_oai_harvester.alma_harvester import UBS_SLSP_OAI

class ecodicesOAI(ePortalOAI):

    oai_endpoint = "https://e-codices.ch/oai/oai.php"

    def __init__(self, *args, **kwargs):
        self.marc_rule = kwargs["marc_rule"]
        super().__init__(*args, **kwargs)

    def harvest_dc(self, oai_set, harvest_from="", harvest_until=""):
        return self.harvest_oai(oai_set=oai_set, harvest_from=harvest_from, harvest_until=harvest_until,
                                oai_metadata="oai_dc", xml_process_func=self.process_ecodices_dc)

    def process_tei_xmls(self, tei_url):
        text = requests.get(tei_url).text
        root = ElementTree.fromstring(text)

    def add_catalog_links(self, bsiz_id, record):
        links = {}
        links["swisscovery_Link"] = [{"link": "https://basel.swisscovery.org/discovery"
                                              "/fulldisplay?docid=alma{}&context=L&vid=41SLSP_UBS:live"
            .format(bsiz_id), "label": "swisscovery"}]
        links["swisscollections_Link"] = [{"link": "https://swisscollections.ch/Record/{}"
            .format(bsiz_id), "label": "swisscovery"}]
        for doi in record.get(DOI_ESFIELD, []):
            links.setdefault("doi_links", []).append({"link": "https://doi.org/{}".format(doi), "label": "DOI zur Originalplattform"})
        return links

    def prepare_digibas_data(self, root, **kwargs):
        id_, oai_xml_data = self.process_ecodices_dc(root, **kwargs)
        if id_:
            if self.marc_access == "oai":
                xml_rec_data = self.slsp_oai.load_oai_recordcache(bsiz_id=id_)
                al =AlmaIZMarcJSONRecord.get_rec_from_xml(xml_rec_data.decode("utf-8"))
            else:
                al =AlmaIZMarcJSONRecord.get_sysid(id_, cachedecorator=self.cachedecorator)
            if al:
                oai_xml_data.update(al.get_main_info())
                oai_xml_data.update(self.marc_rule.transform_marc_record(marc_record=al))
                oai_xml_data.update(self.add_catalog_links(id_, oai_xml_data))
            return id_, oai_xml_data
        else:
            return id_, oai_xml_data

    def process_ecodices_dc(self, root, **kwargs):
        oai_xml_data = {}

        for preview_elem in root.iterfind(".//{http://purl.org/dc/elements/1.1/}preview_page"):
            #https://www.e-codices.unifr.ch/loris/ubb/ubb-A-IV-0037/ubb-A-IV-0037_0013r.jp2/full/,150/0/default/jpg
            oai_xml_data.setdefault(IIIF_PREVIEW_ESFIELD,[]).append(preview_elem.text.split("/full")[0])

        for relation_elem in root.iterfind(".//{http://purl.org/dc/elements/1.1/}relation"):
            rel_text = relation_elem.text
            if rel_text.startswith("IIIF Manifest: "):
                oai_xml_data.setdefault(IIIF_MANIF_ESFIELD,[]).append(rel_text.replace("IIIF Manifest: ", ""))
            elif "/xml/tei_published/" in rel_text:
                oai_xml_data["tei_xml"] = re.search("(?P<tei_link>http.*?\.xml)", rel_text).groupdict()["tei_link"]
            elif "request=" in rel_text:
                old_sysid = re.search("(?P<old_sysid>\d{9})", rel_text).groupdict()["old_sysid"]
                oai_xml_data.setdefault(OLDSYS_ESFIELD,[]).append("5_{}".format(old_sysid))
                al = AlmaIZMarcJSONRecord.get_sysid(oai_xml_data[OLDSYS_ESFIELD][0], cachedecorator=self.cachedecorator)
                if al:
                    oai_xml_data[IZID_ESFIELD] = al.get_primary_id()

        ids = []
        for identifier in root.iterfind(".//{http://purl.org/dc/elements/1.1/}identifier"):
            ids.append(identifier.text)
        for id_ in ids:
            if id_.startswith("doi:"):
                doi = id_.replace("doi:", "")
                oai_xml_data[DOI_ESFIELD] = [doi]
        if not IZID_ESFIELD in oai_xml_data:
            pass
            # TODO lookup für izids
            # Fehlende Sysid-Verweise
            # https://www.e-codices.ch/en/list/one/ubb/R-III-0003
            # https://www.e-codices.ch/en/list/one/ubb/R-IV-0002
            # https://www.e-codices.ch/en/list/one/ubb/R-III-0003
            # https://www.e-codices.ch/en/list/one/ubb/AN-IX-0004
            # https://www.e-codices.ch/en/list/one/ubb/0001
            return None, oai_xml_data
        else:
            return oai_xml_data[IZID_ESFIELD], oai_xml_data