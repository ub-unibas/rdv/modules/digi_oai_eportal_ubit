from digi_oai_eportal_ubit.digi_oai_harvester.oai_harvester import ePortalOAI

from rdv_marc_ubit import AlmaIZMarcJSONRecord
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD, IIIF_IMGS_ESFIELD, IIIF_IMG_COUNT,\
    OLDSYS_ESFIELD, IZID_ESFIELD, DOI_ESFIELD
from rdv_data_helpers_ubit.projects.oai_harvester.oai_harvester import OAI_ID, OAI_DATESTAMP

class eraraOAI(ePortalOAI):
    iiif_img_link_template = "https://www.e-rara.ch/i3f/v20/{}/"
    oai_endpoint = "https://www.e-rara.ch/oai"

    def __init__(self, *args, **kwargs):
        self.marc_rule = kwargs.get("marc_rule")
        super().__init__(*args, **kwargs)


    def harvest_mets(self, oai_set, harvest_from="", harvest_until=""):
        return self.harvest_oai(oai_set=oai_set, harvest_from=harvest_from, harvest_until=harvest_until,
                                oai_metadata="mets", xml_process_func=self.process_eportal_mets)

    def harvest_mods(self, oai_set, harvest_from="", harvest_until=""):
        return self.harvest_oai(oai_set=oai_set, harvest_from=harvest_from, harvest_until=harvest_until,
                                oai_metadata="mods", xml_process_func=self.process_eportal_mods)

    def harvest_dc(self, oai_set, harvest_from="", harvest_until=""):
        return self.harvest_oai(oai_set=oai_set, harvest_from=harvest_from, harvest_until=harvest_until,
                                oai_metadata="oai_dc", xml_process_func=self.process_eportal_dc)

    @classmethod
    def build_iiif_img_link(cls, flocat):
        image_id = flocat.attrib["{http://www.w3.org/1999/xlink}href"].split("/")[-1]
        iiif_img_link = cls.iiif_img_link_template.format(image_id)
        return iiif_img_link

    def process_eportal_dc(self, root, **kwargs):
        oai_xml_data = {}
        ids = []
        for identifier in root.iterfind(".//{http://purl.org/dc/elements/1.1/}identifier"):
            ids.append(identifier.text)
        for id_ in ids:
            if id_.startswith("system:"):
                oai_xml_data[IZID_ESFIELD] = id_.replace("system:", "")
            elif id_.startswith("doi:"):
                doi = id_.replace("doi:", "")
                oai_xml_data[DOI_ESFIELD] = doi
                oai_xml_data["doi_link"] = "http://dx.doi.org/{}".format(doi)
            elif "e-rara.ch" in id_:
                oai_xml_data["link"] = id_
        if not IZID_ESFIELD in oai_xml_data:
            return None, oai_xml_data
        else:
            return oai_xml_data[IZID_ESFIELD], oai_xml_data

    @classmethod
    def process_eportal_mods(cls, root, **kwargs):
        oai_xml_data = {}

        for doi in root.iterfind(".//{http://www.loc.gov/mods/v3}identifier"):
            oai_xml_data.setdefault(DOI_ESFIELD,[]).append(doi.text)
        for sys_id in root.iterfind(".//{http://www.loc.gov/mods/v3}recordIdentifier"):
            if sys_id.startswith("99"):
                oai_xml_data[IZID_ESFIELD] = sys_id
            elif sys_id.startswith("00") and len(sys_id) == 9:
                oai_xml_data[OLDSYS_ESFIELD] = sys_id
        for callnumber in root.iterfind(".//{http://www.loc.gov/mods/v3}shelfLocator"):
            oai_xml_data.setdefault("digi_signatur", []).append(callnumber.text)

        if not IZID_ESFIELD in oai_xml_data:
            return None, oai_xml_data
        else:
            return oai_xml_data[IZID_ESFIELD], oai_xml_data

    @classmethod
    def process_eportal_mets(cls, root, **kwargs):
        oai_xml_data = {}

        # oai:www.e-rara.ch:30716
        oai_id_num = kwargs[OAI_ID].split(":")[-1]

        for dmdSec in root.iterfind(".//{http://www.loc.gov/METS/}dmdSec"):
            if dmdSec.attrib["ID"] == "md{}".format(oai_id_num):
                # DOI
                for doi in dmdSec.iterfind(".//{http://www.loc.gov/mods/v3}identifier"):
                    oai_xml_data.setdefault(DOI_ESFIELD,[]).append(doi.text)
                # IZID bzw. alte Systemnummer
                for sys_id in dmdSec.iterfind(".//{http://www.loc.gov/mods/v3}recordIdentifier"):
                    if sys_id.text.startswith("99"):
                        oai_xml_data[IZID_ESFIELD] = sys_id.text
                    elif sys_id.text.startswith("00") and len(sys_id.text) == 9:
                        oai_xml_data[OLDSYS_ESFIELD] = sys_id.text
                # Signatur
                for callnumber in dmdSec.iterfind(".//{http://www.loc.gov/mods/v3}shelfLocator"):
                    oai_xml_data.setdefault("digi_signatur", []).append(callnumber.text)

        for amdSec in root.iterfind(".//{http://www.loc.gov/METS/}amdSec"):
            if amdSec.attrib["ID"] == "amd{}".format(oai_id_num):
                # IIIF-Manifest
                for iiif_manifest in amdSec.iterfind(".//{http://dfg-viewer.de/}iiif"):
                    oai_xml_data.setdefault(IIIF_MANIF_ESFIELD, []).append(iiif_manifest.text)

        for filegrp in root.iterfind(".//{http://www.loc.gov/METS/}fileGrp"):
            if filegrp.attrib.get("USE") in ["FRONTIMAGE", "TEASER"]:
                for flocat in filegrp.iterfind(".//{http://www.loc.gov/METS/}FLocat"):
                    oai_xml_data.setdefault(IIIF_PREVIEW_ESFIELD, []).append(cls.build_iiif_img_link(flocat))
            if filegrp.attrib.get("USE") == "DEFAULT":
                for flocat in filegrp.iterfind(".//{http://www.loc.gov/METS/}FLocat"):
                    oai_xml_data.setdefault(IIIF_IMGS_ESFIELD, []).append(cls.build_iiif_img_link(flocat))
                oai_xml_data.setdefault(IIIF_IMG_COUNT, len(oai_xml_data[IIIF_IMGS_ESFIELD]))
        if not IZID_ESFIELD in oai_xml_data:
            return None, oai_xml_data
        else:
            return oai_xml_data[IZID_ESFIELD], oai_xml_data

    def add_catalog_links(self, bsiz_id, record):
        links = {}
        links["swisscovery_Link"] = [{"link": "https://basel.swisscovery.org/discovery"
                                              "/fulldisplay?docid=alma{}&context=L&vid=41SLSP_UBS:live"
            .format(bsiz_id), "label": "swisscovery"}]
        links["swisscollections_Link"] = [{"link": "https://swisscollections.ch/Record/{}"
            .format(bsiz_id), "label": "swisscovery"}]
        for doi in record.get(DOI_ESFIELD, []):
            links.setdefault("doi_links", []).append({"link": "https://doi.org/{}".format(doi), "label": "DOI zur Originalplattform"})
        return links

    def prepare_digibas_data(self, root, **kwargs):
        id_, oai_xml_data = self.process_eportal_mets(root, **kwargs)
        if id_:
            if self.marc_access == "oai":
                xml_rec_data = self.slsp_oai.load_oai_recordcache(bsiz_id=id_)
                al =AlmaIZMarcJSONRecord.get_rec_from_xml(xml_rec_data.decode("utf-8"))
            else:
                al =AlmaIZMarcJSONRecord.get_sysid(id_, cachedecorator=self.cachedecorator)
            if al:
                oai_xml_data.update(al.get_main_info())
                oai_xml_data.update(self.marc_rule.transform_marc_record(marc_record=al))
                oai_xml_data.update(self.add_catalog_links(id_, oai_xml_data))
            return id_, oai_xml_data
        else:
            return id_, oai_xml_data

class emanuscriptaOAI(eraraOAI):
    iiif_img_link_template = "https://www.e-manuscripta.ch/i3f/v21/{}/"
    oai_endpoint = "https://www.e-manuscripta.ch/oai"

