from xml.etree import ElementTree
from pkg_resources import Requirement, resource_filename

from digi_oai_eportal_ubit.digi_oai_harvester import ePortalOAI
from rdv_marc_ubit import AlmaIZMarcJSONRecord
from cache_decorator_redis_ubit import CacheDecorator
from mongo_decorator_ubit import create_mongo_decorator
from es_ingester_ubit import IngestEs
from rdv_data_helpers_ubit import IZID_ESFIELD, autocomplete_mapping, autocomplete_settings
from elasticsearch import Elasticsearch

class UBS_SLSP_OAI(ePortalOAI):
    """
    Class for harvesting metadata from the SLSP UBS ePortal using OAI-PMH protocol.
    """
    oai_endpoint = "https://eu03.alma.exlibrisgroup.com/view/oai/41SLSP_UBS/request"

    def __init__(self, *args, **kwargs):
        self.oai_set = kwargs["oai_set"] if kwargs.get("oai_set") else ""
        self.oai_metadata = kwargs["oai_metadata"] if kwargs.get("oai_metadata") else ""
        self.es_host = kwargs["es_host"] if kwargs.get("es_host") else ""
        super().__init__(*args, **kwargs)
        if not self.slsp_oai:
            self.slsp_oai = self
            self.marc_access = "oai"

    def load_oai_recordcache(self, oai_id=None, bsiz_id=None, oai_metadata=None):
        """
        Loads an OAI record from cache or retrieves it from the OAI provider if it is not already cached.

        Args:
            oai_id (str, optional): The OAI identifier of the OAI record to load. Example oai:alma.41SLSP_UBS:9966658440105504
            bsiz_id (str, optional): The identifier of the record in the bibliographic data store. Example 9966658440105504
            oai_metadata (str, optional): The metadata prefix to use when retrieving the OAI record.

        Returns:
            str: The OAI record as a string.

        Example:
            record = obj.load_oai_recordcache(oai_id='oai:alma.41SLSP_UBS:9966658440105504')
        """

        params = {"oai_id": oai_id, "oai_metadata": oai_metadata or self.oai_metadata} \
            if oai_id \
            else {"bsiz_id": bsiz_id, "oai_metadata": oai_metadata or self.oai_metadata}
        oai_record_cache_key = self.build_oai_record_id(**params)
        record_str = self.oai_store.get_keyvalue_cache(key=oai_record_cache_key)
        if not record_str:
            oai_id = self.build_ubs_oai_id(bsiz_id) if bsiz_id else oai_id
            record = self.sickle.GetRecord(identifier=oai_id, metadataPrefix=oai_metadata or self.oai_metadata)
            record_str = str(record).encode("utf-8")
            self.oai_store.set_keyvalue_cache(key=oai_record_cache_key, value=record_str)
        return record_str

    def incremental_harvest_slsp(self):

        harvested_oai_ids, deleted_oai_ids = self.get_harvested_ids(self.oai_set, self.oai_metadata, incremental=True)
        self.logger.warning("Incremental Harvest: {} ids to be processed (only first 100): {}".format(len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))

        return harvested_oai_ids, deleted_oai_ids

    def build_oai_record_id(self, oai_id=None, bsiz_id=None, oai_metadata=None):
        if oai_id:
            ubs_oai_id = oai_id
        else:
            ubs_oai_id = self.build_ubs_oai_id(bsiz_id)
        oai_cache_rec_id = "{}_{}_{}".format(self.oai_endpoint, oai_metadata or self.oai_metadata, ubs_oai_id)
        return oai_cache_rec_id

    def build_ubs_oai_id(self, bsiz_id):
        return "oai:alma.41SLSP_UBS:{}".format(bsiz_id)

    def process_marc(cls, root, **kwargs):
        oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
        if oai_header and oai_header.attrib.get("status") == "deleted":
            return None, None
        xml_data = ElementTree.tostring(root).decode("utf-8")
        al = AlmaIZMarcJSONRecord.get_rec_from_xml(xml_data)
        entry = al.get_main_info()
        if not IZID_ESFIELD in entry:
            return None, entry
        else:
            return entry[IZID_ESFIELD], entry

    def harvest_and_process(self, incremental=False, reharvest=False, xml_process_func=lambda x: x, last_harvest_date=None, testset=None):
        """
        Harvest and process the data
        :param incremental (boolean) : if True, only harvest from last_harvest_data
        :param reharvest (boolean) : if True, reharvest the data from the oai endpoint
        :param xml_process_func (function) : function with which to process each record
        :param last_harvest_date (string, format 2023-04-24): last harvest date, used only in incremental mode
        :param testset (integer) : only the first testset records will be processed
        :return: a dictionary containing all records
        """
        oai_portal_data = {}
        if incremental:
            oai_portal_data.update(self.incremental_harvest(oai_set=self.oai_set, oai_metadata=self.oai_metadata,
                                                            xml_process_func=xml_process_func, last_harvest_date=last_harvest_date, testset=testset))
        elif reharvest:
            oai_portal_data.update(self.full_harvest(oai_set=self.oai_set, oai_metadata=self.oai_metadata,
                                                     xml_process_func=xml_process_func, last_harvest_date=last_harvest_date, testset=testset))
        else:
            oai_portal_data.update(self.full_cached_harvest(oai_set=self.oai_set, oai_metadata=self.oai_metadata,
                                                            xml_process_func=xml_process_func, last_harvest_date=last_harvest_date, testset=testset))
        return oai_portal_data

    def ingest2index(self, sub_index, index_prefix, incremental=False, reharvest=False, xml_process_func=lambda x: x, last_harvest_date=None):
        oai_portal_data = self.harvest_and_process(incremental=incremental, reharvest=reharvest,
                                                   xml_process_func=xml_process_func, last_harvest_date=last_harvest_date)
        body = {"settings": autocomplete_settings, "mappings": autocomplete_mapping}
        ingest = IngestEs(Elasticsearch(self.es_host, timeout=200), index_name=self.index, sub_index=sub_index,
                          body=body,
                          mapping=autocomplete_mapping, index_prefix=index_prefix, incremental=incremental, logger=self.logger)
        ingest.load_data(oai_portal_data)
        ingest.alias_new_index()

if __name__ == "__main__":
    MongoDecorator = create_mongo_decorator(config_file=resource_filename(Requirement.parse("ubunibas_mongo"), "ubunibas_mongo/cfg/mongo_emptyvalues.yaml"))

    alma_oai = UBS_SLSP_OAI(oai_sets={}, cachedecorator=CacheDecorator, oai_store=MongoDecorator(db_name="slsp_oai_harvesting", collection_name="retrodigi"))
    if 0:
        alma_data = alma_oai.full_cached_harvest(oai_set="retrodigi", oai_metadata="marc21", xml_process_func=alma_oai.process_marc, last_harvest_date="2022-01-05")

    if 1:
        alma_data = alma_oai.harvest_ids(oai_ids=["oai:alma.41SLSP_UBS:9942957960105504"], oai_set="retrodigi",
                                         oai_metadata="marc21", xml_process_func=alma_oai.process_marc)
    print(alma_data)
