import datetime
import logging
from xml.etree import ElementTree

import requests
from sickle import Sickle, oaiexceptions

from cache_decorator_redis_ubit import NoCacheDecorator
from rdv_data_helpers_ubit.projects.oai_harvester.oai_harvester import OAI_ID, OAI_DATESTAMP, OAI_SETS

class ePortalOAI:

    def __init__(self, oai_store=NoCacheDecorator(), utils_mongo_store=NoCacheDecorator, cachedecorator=NoCacheDecorator, slsp_oai=None,
                 slsp_dump_file="", oai_sets={}, logger=logging.getLogger("oai_harvester"), **kwargs):
        """

        :param oai_store: Cache or MongoDecorator for storing OAI-Records
        :param cachedecorator: CacheDecorator for Accessing SRu MARC Records
        :param slsp_oai: OAI-Caching Interface for SLSP OAI
        :param slsp_dump_file: Dump-File containing Marc Data
        :param oai_sets: dictonary containing metadata added to a specific subset
        :param logger:
        :param kwargs:
        """
        self.logger = logger
        self.oai_sets = oai_sets
        self.oai_store = oai_store
        self.utils_mongo_store = utils_mongo_store
        self.cachedecorator = cachedecorator
        self.slsp_oai = slsp_oai
        self.slsp_dump_file = slsp_dump_file
        self.marc_access = "oai" if self.slsp_oai else "dump"
        self.sickle = Sickle(self.oai_endpoint)

    def harvest_ids(self, oai_ids, oai_set, oai_metadata, xml_process_func):
        """
        Harvest records based on an oai id's list. Take it from cache if available
        """
        if not isinstance(oai_ids, list):
            oai_ids =[oai_ids]
        harvested_oai_ids = set()
        deleted_oai_ids = set()
        for oai_id in oai_ids:
            harvested_oai_id, deleted_oai_id = self.harvest_oai_record(identifier=oai_id, oai_metadata=oai_metadata)
            if harvested_oai_id:
                harvested_oai_ids = harvested_oai_ids | harvested_oai_id
            if deleted_oai_id:
                deleted_oai_ids = deleted_oai_ids | deleted_oai_id
        self.logger.warning("ID Harvest: {} ids freshly harvested (only first 100 listed): {}".format(len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))
        self.logger.warning("ID Harvest:{} ids to be processed (only first 100 listed): {}".format(len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))

        return self.process_oai_records(harvested_oai_ids, oai_set, oai_metadata, xml_process_func)

    def get_harvested_ids(self, oai_set, oai_metadata, last_harvest_date=None, incremental=False):
        """
        Get the id's which are available in the cache
        """
        log_inc_prefix = "Incremental Harvest: " if incremental else ""
        harvestdate_store_key = self.build_harvest_date_key(oai_set=oai_set, oai_metadata=oai_metadata)
        last_harvest_date = last_harvest_date or self.oai_store.get_keyvalue_cache(key=harvestdate_store_key)
        self.logger.warning("{}Last Harvest date for {}: {}".format(log_inc_prefix , harvestdate_store_key, last_harvest_date))

        harvested_oai_ids, deleted_oai_ids = self.harvest_oai_records(oai_set=oai_set, harvest_from=last_harvest_date,
                                                                      oai_metadata=oai_metadata)
        self.logger.warning("{}{} ids freshly harvested (only first 100 listed): {}".format(log_inc_prefix, len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))

        harvest_date = datetime.datetime.today().strftime("%Y-%m-%d")
        self.oai_store.set_keyvalue_cache(key=harvestdate_store_key, value=harvest_date)
        return harvested_oai_ids, deleted_oai_ids

    def incremental_harvest(self, oai_set, oai_metadata, xml_process_func, last_harvest_date=None, testset=None):
        """
        Incremental harvest from oai provider based on last_harvest_data. Don't use the cache at all.
        """
        harvested_oai_ids, deleted_oai_ids = self.get_harvested_ids(oai_set, oai_metadata, last_harvest_date, incremental=True)
        self.logger.warning("Incremental Harvest: {} ids to be processed (only first 100 listed): {}".format(len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))

        return self.process_oai_records(harvested_oai_ids, oai_set, oai_metadata, xml_process_func, testset=testset)


    def full_cached_harvest(self, oai_set, oai_metadata, xml_process_func, last_harvest_date=None, testset=None):
        """
        Use all records which are in the cache
        """
        harvested_oai_ids, deleted_oai_ids = self.get_harvested_ids(oai_set, oai_metadata, last_harvest_date)
        all_oai_ids = self.get_all_oai_ids(oai_set=oai_set, oai_metadata=oai_metadata,
                                           new_oai_ids=harvested_oai_ids, deleted_oai_ids=deleted_oai_ids)
        self.logger.warning("{} ids to be processed (only first 100 listed): {}".format(len(all_oai_ids), list(all_oai_ids)[0:100]))

        return self.process_oai_records(all_oai_ids, oai_set, oai_metadata, xml_process_func, testset=testset)

    def full_harvest(self, oai_set, oai_metadata, xml_process_func, last_harvest_date=None, testset=None):
        last_harvest_date = "1970-01-01" if not last_harvest_date else last_harvest_date
        harvested_oai_ids, deleted_oai_ids = self.get_harvested_ids(oai_set, oai_metadata, last_harvest_date=last_harvest_date)
        oai_ids_store_key = self.build_allids_key(oai_set, oai_metadata)
        self.oai_store.set_keyvalue_cache(key=oai_ids_store_key, value=harvested_oai_ids)
        self.logger.warning("Full Harvest: {} ids to be processed (only first 100 listed): {}".format(len(harvested_oai_ids), list(harvested_oai_ids)[0:100]))

        return self.process_oai_records(harvested_oai_ids, oai_set, oai_metadata, xml_process_func, testset=testset)

    def get_xml_record(self, oai_id, oai_metadata):
        self.harvest_oai_record(identifier=oai_id, oai_metadata=oai_metadata)
        record_xml_str = self.load_oai_recordcache(oai_id=oai_id, oai_metadata=oai_metadata).decode("utf-8")
        return record_xml_str

    def process_oai_records(self, harvested_oai_ids, oai_set, oai_metadata, xml_process_func, testset=None):
        processed_oai_data = {}
        for n, oai_id in enumerate(harvested_oai_ids):
            if n % 1000 == 0:
                self.logger.warning("{} records processed {}.".format(n, datetime.datetime.now()))
            if testset and n == testset:
                self.logger.info("TESTSET: {} records processed {}. Processing stopped".format(n, datetime.datetime.now()))
                return processed_oai_data
            record = self.load_oai_recordcache(oai_id=oai_id, oai_metadata=oai_metadata)
            try:
                id_, entry  = self.process_record(oai_set=oai_set, record=record, xml_process_func=xml_process_func)
            except IndexError:
                id_, entry  = None, None
                import traceback
                traceback.print_exc()
            if id_:
                processed_oai_data[id_] = entry
            else:
                self.logger.debug("Filtered / or No ID for {}".format(entry))
        return processed_oai_data


    def load_oai_recordcache(self, oai_id, oai_metadata):
        oai_record_cache_key = self.build_oai_record_id(oai_id=oai_id, oai_metadata=oai_metadata)
        record_str = self.oai_store.get_keyvalue_cache(key=oai_record_cache_key)
        if not record_str:
            record = self.sickle.GetRecord(identifier=oai_id, metadataPrefix=oai_metadata)
            record_str = str(record).encode("utf-8")
            self.oai_store.set_keyvalue_cache(key=oai_record_cache_key, value=record_str)
        return record_str

    def process_record(self, oai_set, record, xml_process_func):
        root = ElementTree.fromstring(record)
        oai_id = root.find(".//{http://www.openarchives.org/OAI/2.0/}identifier").text
        oai_datestamp = root.find(".//{http://www.openarchives.org/OAI/2.0/}datestamp").text
        oai_sets = []
        for setspec in root.findall(".//{http://www.openarchives.org/OAI/2.0/}setSpec"):
            oai_sets.append(setspec.text)
        entry = {OAI_ID: oai_id, OAI_DATESTAMP: oai_datestamp, OAI_SETS: oai_sets}
        entry.update(self.oai_sets.get(oai_set, {}))
        id_, xml_data = xml_process_func(root=root, **entry)
        if xml_data:
            entry.update(xml_data)
        return id_, entry

    def build_harvest_date_key(self, oai_set, oai_metadata):
        harvestdate_store_key = "harvest_date_{}_{}_{}".format(self.oai_endpoint, oai_set, oai_metadata)
        return harvestdate_store_key

    def build_oai_record_id(self, oai_id, oai_metadata):
        oai_cache_rec_id = "{}_{}_{}".format(self.oai_endpoint, oai_metadata, oai_id)
        return oai_cache_rec_id

    def build_allids_key(self, oai_set, oai_metadata):
        allids_key = "oai_ids_{}_{}_{}".format(self.oai_endpoint, oai_set, oai_metadata)
        return allids_key

    def build_del_allids_key(self, oai_set, oai_metadata):
        allids_key = "del_oai_ids_{}_{}_{}".format(self.oai_endpoint, oai_set, oai_metadata)
        return allids_key

    def get_all_oai_ids(self, oai_set, oai_metadata, new_oai_ids=set(), deleted_oai_ids=set()):
        store_key = self.build_allids_key(oai_set, oai_metadata)
        all_oai_ids = self.oai_store.get_keyvalue_cache(key=store_key) or set()
        all_oai_ids = (new_oai_ids | all_oai_ids) - deleted_oai_ids
        self.oai_store.set_keyvalue_cache(key=store_key, value=all_oai_ids)
        return all_oai_ids

    def get_all_del_oai_ids(self, oai_set, oai_metadata, new_oai_ids=set(), deleted_oai_ids=set()):
        store_key = self.build_del_allids_key(oai_set, oai_metadata)
        all_del_oai_ids = self.oai_store.get_keyvalue_cache(key=store_key) or set()
        all_del_oai_ids = (all_del_oai_ids | deleted_oai_ids) - new_oai_ids
        self.oai_store.set_keyvalue_cache(key=store_key, value=all_del_oai_ids)
        return all_del_oai_ids


    def harvest_oai_records(self, oai_set, oai_metadata, harvest_from="", harvest_until=""):

        harvest_period = {"from": harvest_from if harvest_from else "1970-01-01",
                          "until": harvest_until if harvest_until else "2050-12-31"}

        oai_ids = set()
        deleted_oai_ids = set()
        try:
            records = self.sickle.ListRecords(metadataPrefix=oai_metadata, set=oai_set, **harvest_period)
            self.logger.warning("OAI-Harvest {} {} {} {from} {until}".format(self.oai_endpoint, oai_metadata, oai_set, **harvest_period))
            for n, record in enumerate(records):
                if n % 1000 == 0:
                    self.logger.warning("{} records loaded {}.".format(n, datetime.datetime.now()))
                record_str = str(record).encode("utf-8")
                root = ElementTree.fromstring(record_str)
                oai_id = root.find(".//{http://www.openarchives.org/OAI/2.0/}identifier").text
                oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
                store_key = self.build_oai_record_id(oai_id=oai_id, oai_metadata=oai_metadata)
                if oai_header and oai_header.attrib.get("status") == "deleted":
                    self.oai_store.del_keyvalue_cache(key=store_key)
                    deleted_oai_ids.add(oai_id)
                else:
                    self.oai_store.set_keyvalue_cache(key=store_key, value=record_str)
                    oai_ids.add(oai_id)
        except requests.exceptions.HTTPError as e:
            # TODO: auch 403 CLient error wird davon abgefangen
            import traceback
            traceback.print_exc()
            self.logger.error("Daten via resumption Token für OAI konnten nicht geladen werden {}".format(e))
        except oaiexceptions.NoRecordsMatch as e:
            self.logger.info("Keine Records konnten geharvestet werden {}".format(e))
        return oai_ids, deleted_oai_ids


    def harvest_oai_record(self, identifier, oai_metadata):
        oai_ids = set()
        deleted_oai_ids = set()
        record = self.sickle.GetRecord(identifier=identifier, metadataPrefix=oai_metadata)
        record_str = str(record).encode("utf-8")
        root = ElementTree.fromstring(record_str)
        oai_id = root.find(".//{http://www.openarchives.org/OAI/2.0/}identifier").text
        oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
        store_key = self.build_oai_record_id(oai_id=oai_id, oai_metadata=oai_metadata)
        if oai_header and oai_header.attrib.get("status") == "deleted":
            deleted_oai_ids.add(oai_id)
            self.oai_store.del_keyvalue_cache(key=store_key)
        else:
            self.oai_store.set_keyvalue_cache(key=store_key, value=record_str)
            oai_ids.add(oai_id)

        self.oai_store.set_keyvalue_cache(key=store_key, value=record_str)

        return oai_ids, deleted_oai_ids

    def harvest_oai_ids(self, oai_set, oai_metadata, harvest_from="", harvest_until=""):
        harvest_period = {"from": harvest_from if harvest_from else "2000-01-01",
                          "until": harvest_until if harvest_until else "2050-12-31"}
        records = self.sickle.ListIdentifiers(set=oai_set, metadataPrefix=oai_metadata, ignore_deleted=True, **harvest_period)
        oai_ids = set()
        deleted_oai_ids = set()
        try:
            for n, oai_id_header in enumerate(records):
                oai_id_header_str = str(oai_id_header).encode("utf-8")
                root = ElementTree.fromstring(oai_id_header_str)
                oai_id = root.find(".//{http://www.openarchives.org/OAI/2.0/}identifier").text
                oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
                if oai_header and oai_header.attrib.get("status") == "deleted":
                    store_key = self.build_oai_record_id(oai_id=oai_id, oai_metadata=oai_metadata)
                    self.oai_store.del_keyvalue_cache(key=store_key)
                    deleted_oai_ids.add(oai_id)
                else:
                    self.load_oai_recordcache(oai_id=oai_id, oai_metadata=oai_metadata)
                    oai_ids.add(oai_id)
        except requests.exceptions.HTTPError as e:
            import traceback
            traceback.print_exc()
            self.logger.error("Daten via resumption Token für OAI konnten nicht geladen werden {}".format(e))
        return oai_ids, deleted_oai_ids
